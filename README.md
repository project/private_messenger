Private Messenger
=================================


CONTENTS
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

 * This module extends functionality of core messenger to handle situations
   when you want to leave a specific user with warning/error/info message on
   login event.

 * If system has applied an action against a user or any data and
  you want to let know what happened beside any other notification method.

 * I think this module can be more developed to not just leave a message on
   login but also on page refresh but I didn't check on performance if did so
   and admin may leave a custom warning for specific user through a form. 
   If you have an idea to extend, you're more than welcomed to create an issue.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/private_messenger

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/private_messenger 

REQUIREMENTS
------------

 * This module requires no modules outside of Drupal core.


INSTALLATION
------------

* To get the module you can download and unzip it in contrib modules folder or
  better use composer to install the module and manage your dependencies:
```
 $ composer require drupal/private_messenger.
```
* visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * You need to create an private messenger entity whenever needed and module
   will load this entity on login and show message and remove the entity to not
   flood its table. in future i will add a simpler method to send the message.
    
 ```
  $message = \Drupal::entityTypeManager()->getStorage('private_messenger_message')->create([
    'type' => 'warning',
    'recepient_uid' => 5,
    'message' => 'Dear @name, please update your profile info asap.'
  ]);
  // Optional parameters in message.
  $message->setParameters(['@name' => 'name')]);
  $message->save();
 ```

  or from UI by going to /admin/content/private-messenger-message/add
  but keep in mind that you have to serialize the parameters array
  first before passing it to parameters input.

IMOPORTANT NOTE
-------------

 * If you use version 1.3.0, you can send the message as 
 'message' => 'please update your profile info asap.', 
but in version 1.2.0 you have to send it within translation function
 'message' => t('please update your profile info asap.'), 
