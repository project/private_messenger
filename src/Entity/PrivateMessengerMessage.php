<?php

namespace Drupal\private_messenger\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\private_messenger\PrivateMessengerMessageInterface;
use Drupal\user\UserInterface;

/**
 * Defines the private messenger message entity class.
 *
 * @ContentEntityType(
 *   id = "private_messenger_message",
 *   label = @Translation("Private Messenger Message"),
 *   label_collection = @Translation("Private Messenger Messages"),
 *   handlers = {
 *     "view_builder" = "Drupal\private_messenger\PrivateMessengerMessageViewBuilder",
 *     "list_builder" = "Drupal\private_messenger\PrivateMessengerMessageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\private_messenger\Form\PrivateMessengerMessageForm",
 *       "edit" = "Drupal\private_messenger\Form\PrivateMessengerMessageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "private_messenger_message",
 *   admin_permission = "access private messenger message overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/private-messenger-message/add",
 *     "canonical" = "/private_messenger_message/{private_messenger_message}",
 *     "edit-form" = "/admin/content/private-messenger-message/{private_messenger_message}/edit",
 *     "delete-form" = "/admin/content/private-messenger-message/{private_messenger_message}/delete",
 *     "collection" = "/admin/content/private-messenger-message"
 *   },
 * )
 */
class PrivateMessengerMessage extends ContentEntityBase implements PrivateMessengerMessageInterface {

  /**
   * {@inheritdoc}
   *
   * When a new private messenger message entity is created,
   * set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecepient() {
    return $this->get('recepient_uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecepientId() {
    return $this->get('recepient_uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecepientId($uid) {
    $this->set('recepient_uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecepient(UserInterface $account) {
    $this->set('recepient_uid', $account->id());
    return $this;
  }

  /**
   * Gets original message text.
   */
  public function getMessage() {
    return $this->get('message')->value;
  }

  /**
   * Message text and is translated from ui or po.
   */
  public function setMessage($value) {
    $this->set('message', $value);
    return $this;
  }

  /**
   * Gets original message text.
   *
   * @return array
   *   Return message parameters.
   */
  public function getParameters() {
    $value = $this->get('parameters')->value;
    $value = unserialize($value);
    return $value;
  }

  /**
   * Sets message parameters.
   *
   * @param array $value
   *   Array to set as parameters.
   */
  public function setParameters(array $value) {
    foreach ($value as &$str) {
      if ($str instanceof TranslatableMarkup) {
        $str = $str->render();
      }
    }
    $value = serialize($value);
    $this->set('parameters', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('A type of the private messenger message (warning, status, error).'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['message'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Message'))
      ->setDescription(t('A description of the private messenger message.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['parameters'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parameters'))
      ->setDescription(t('parameters of the private messenger message.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the private messenger message author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['recepient_uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recepient user'))
      ->setDescription(t('The user ID of the private messenger message recepient.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the private messenger message was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Set the 'max-age' to 0 to prevent caching.
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

}
