<?php

namespace Drupal\private_messenger;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a private messenger message entity type.
 */
interface PrivateMessengerMessageInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the private messenger message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the private messenger message.
   */
  public function getCreatedTime();

  /**
   * Sets the private messenger message creation timestamp.
   *
   * @param int $timestamp
   *   The private messenger message creation timestamp.
   *
   * @return \Drupal\private_messenger\PrivateMessengerMessageInterface
   *   The called private messenger message entity.
   */
  public function setCreatedTime($timestamp);

}
